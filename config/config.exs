# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Configures the endpoint
config :vimwiki_deliver, VimwikiDeliverWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "n80UpBpDhAfXgy+HW4raOIwrFt/S2OiiYVZmLsRBDFWso1cWa+O7llja819bd7qC",
  render_errors: [view: VimwikiDeliverWeb.ErrorView, accepts: ~w(json)],
  pubsub: [name: VimwikiDeliver.PubSub, adapter: Phoenix.PubSub.PG2],
  api_key: "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx"

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Timezone stuff
config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

# http adapter stuff for making calls to telegram
config :tesla, adapter: Tesla.Adapter.Hackney

config :vimwiki_deliver, VimwikiDeliver.Telegram.Api,
  http_client: Tesla

# Telegram API Config
config :vimwiki_deliver, VimwikiDeliver.Telegram.Api,
  token: "xxxxxxxxxx",
  url: "https://mockbin.org/echo/"

config :vimwiki_deliver, VimwikiDeliver.Sessions,
  admin_id: 111111111

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
