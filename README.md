# VimwikiDeliver

Vimwiki Deliver seeks to solve the problem adding to a personal wiki remotly.

> you can check the [CHANGELOG](CHANGELOG.md) to see all the releases

Main components:

- Telegram bot named [Vimwiki Delivery Bot](https://t.me/vimwiki_delivery_bot)
- Chatbot backend, Elixir + Phoenix
    - mainly revolving around a queue I implemented in OTP see [queue.ex](lib/vimwiki_deliver/queue.ex)
    - the app is deployed via docker + kubernetes
- Perl script that curls the backend and inserts entries into a Incoming.wiki file


Screenshot:

![chat bot screen shot](/doc_assets/phone_screenshot.jpg)

## .vimrc

In order to automatically dump when you open `Incoming.wiki` add this to your `.vimrc`

```vimscript
:autocmd BufRead Incoming.wiki ! /Users/----/vimwiki_deliver/dump.pl --path  /Users/----/vimwiki/Incoming.wiki --apikey xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
```

## Incoming.wiki sample

```plaintext
= Incoming =

=== VimWiki Delivery 23-03-2020 20:05 ===
you have 5 new enteries!

  - [ ] init games list?  Zero Escape - Wikipedia https://en.wikipedia.org/wiki/Zero_Escape
  - [ ] Inspiration for vimwiki delivery project: https://www.jakerobers.com/a-simple-tool-memento
  - [ ] Tech blog inspiration -- http://tenderlovemaking.com/
  - [ ] online book tour -- https://www.youtube.com/user/theartassignment
```

## Inspiration

This project takes heavy inspiration from https://www.jakerobers.com/a-simple-tool-memento

## Dev

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Start Phoenix endpoint with `mix phx.server`
  * Or interactivity `iex -S mix phx.server`
  * also has test that can be run via `mix test`


Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.
