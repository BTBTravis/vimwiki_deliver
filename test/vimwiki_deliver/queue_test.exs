defmodule QueueTest do
  use ExUnit.Case, async: true
  doctest VimwikiDeliver.Queue
  alias VimwikiDeliver.Queue

  setup do
    {:ok, queue} = Queue.start_link([])
    %{queue: queue}
  end

  test "queue starts empty", %{queue: queue} do
    assert Queue.dump(queue) == []
  end

  test "can only add entries to queue", %{queue: queue} do
    put = Queue.put(queue, "test")
    assert elem(put, 0) == :error
  end

  test "adding three entries dumps three, and clears queue", %{queue: queue} do
    {:ok, entry1} = VimwikiDeliver.Entries.create("entry1")
    {:ok, entry2} = VimwikiDeliver.Entries.create("entry2")
    {:ok, entry3} = VimwikiDeliver.Entries.create("entry3")
    :ok = Queue.put(queue, entry1)
    :ok = Queue.put(queue, entry2)
    :ok = Queue.put(queue, entry3)
    dump = Queue.dump(queue)
    assert length(dump) == 3
    assert Enum.fetch!(dump, -1).entry_text == "entry3"
    dump2 = Queue.dump(queue)
    assert length(dump2) == 0
  end
end

