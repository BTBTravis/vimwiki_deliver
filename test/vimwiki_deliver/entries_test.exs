defmodule EntriesTest do
  use ExUnit.Case
  doctest VimwikiDeliver.Entries
  alias VimwikiDeliver.Entries

  test "creates entry form text" do
    {:ok, entry} = Entries.create("test_string")
    assert entry.entry_text == "test_string"
    assert entry.timestamp.time_zone == "Europe/Berlin"
  end
end

