defmodule SessionsTest do
  use ExUnit.Case
  doctest VimwikiDeliver.Sessions
  alias VimwikiDeliver.Sessions

  setup_all do
    raw_data_mock = %{
      "message" => %{
        "chat" => %{
          "first_name" => "Travis",
          "id" => 953058570,
          "last_name" => "Shears",
          "type" => "private"
        },
        "date" => 1506207666,
        "from" => %{
          "first_name" => "Travis",
          "id" => 111111111,
          "is_bot" => false,
          "language_code" => "en-US",
          "last_name" => "Shears"
        },
        "message_id" => 15894,
        "text" => "/help"
      },
      "update_id" => 24063093
    }
    {:ok, telegram_json: raw_data_mock}
  end


  test "creates a session from telegram json", %{telegram_json: telegram_json} do
    expected_session = %Sessions.Session{
      chat_id: 953058570,
      message: "/help",
      responces: [],
      user_id: 111111111,
      is_admin: true
    }

    {:ok, session} = Sessions.create(telegram_json)
    assert session == expected_session
  end

  test "can add message to session", %{telegram_json: telegram_json} do
    expected_session = %Sessions.Session{
      chat_id: 953058570,
      message: "/help",
      responces: ["test_msg"],
      user_id: 111111111,
      is_admin: true
    }

    {:ok, session} = Sessions.create(telegram_json)
    session = Sessions.add_message(session, "test_msg")
    assert session == expected_session
  end

  test "detects non admin", %{telegram_json: telegram_json} do
    expected_session = %Sessions.Session{
      chat_id: 953058570,
      message: "/help",
      responces: ["test_msg"],
      user_id: 222222222,
      is_admin: false
    }
    telegram_json = put_in(telegram_json["message"]["from"]["id"], 222222222)

    {:ok, session} = Sessions.create(telegram_json)
    session = Sessions.add_message(session, "test_msg")
    assert session == expected_session
  end
end

