defmodule BotTest do
  use ExUnit.Case
  doctest VimwikiDeliver.Bot
  alias VimwikiDeliver.Bot

  setup_all do
    raw_data_mock = %{
      "message" => %{
        "chat" => %{
          "first_name" => "Travis",
          "id" => 953058570,
          "last_name" => "Shears",
          "type" => "private"
        },
        "date" => 1506207666,
        "from" => %{
          "first_name" => "Travis",
          "id" => 111111111,
          "is_bot" => false,
          "language_code" => "en-US",
          "last_name" => "Shears"
        },
        "message_id" => 15894,
        "text" => "/help"
      },
      "update_id" => 24063093
    }
    {:ok, telegram_json: raw_data_mock}
  end

  test "handles /start and /help commands", %{telegram_json: telegram_json} do
    res = Bot.handle_incoming_telegram_msg(telegram_json)[:ok]
    assert res.body["chat_id"] == 953058570
    assert Regex.match?(~r/^Hello/, res.body["text"])
  end

  test "handles non admin messages", %{telegram_json: telegram_json} do
    # change to non admin user_id
    telegram_json = put_in(telegram_json["message"]["from"]["id"], 222222222)
    telegram_json = put_in(telegram_json["message"]["chat"]["id"], 333333333)
    res = Bot.handle_incoming_telegram_msg(telegram_json)[:ok]
    assert res.body["chat_id"] == 333333333
    assert Regex.match?(~r/^Oops/, res.body["text"])
  end

  test "saves entry", %{telegram_json: telegram_json} do
    # update message to include book with url to save
    expected_txt = "book https://www.goodreads.com/book/show/929.Memoirs_of_a_Geisha"
    telegram_json = put_in(telegram_json["message"]["text"], expected_txt)
    res = Bot.handle_incoming_telegram_msg(telegram_json)[:ok]
    dump = hd(VimwikiDeliver.Queue.dump(VimwikiDeliver.Queue))
    saved_txt = dump.entry_text
    assert Regex.match?(~r/entry saved/, res.body["text"])
    assert saved_txt == expected_txt
  end

  test "/count command works", %{telegram_json: telegram_json} do
    telegram_json = put_in(telegram_json["message"]["text"], "/count")
    res = Bot.handle_incoming_telegram_msg(telegram_json)[:ok]
    assert Regex.match?(~r/0 enteries/, res.body["text"])
  end
end

