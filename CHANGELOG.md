# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - Sun Mar 22 - 7602751

### Added
- feat(dump_script): add host arg
- feat(dump_script): add script to append dump to Incoming.wiki

### Changed
- feat(dump): kill per entry date printing

## [0.1.0] - Sun Mar 22 - e3f9a2c

### Added
- feat(dump): add template and view
- feat(dump): init endpoint with api_key checking
- feat(count): add /count command and hook up queue
- feat(admin): only accept messages from admin user


## [0.0.1-alpah1] - Sat Mar 21 - 2b632a7
Inital app with sessions, queues, entries, and k8s deploy. Has working `/help` and `/start`
commands. All other messages will simply return "your user_id is #"

### Added
- feat(docker+k8s): init docker + k8s deploy
- feat(sessions): init sessions
- feat(queue): add QueueSupervisor
- feat(queue): init entry queue
- feat(entries): init Entries module
