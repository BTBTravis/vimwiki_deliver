defmodule VimwikiDeliverWeb.AdminController do
  use VimwikiDeliverWeb, :controller

  def send400(conn) do
    conn
    |> send_resp(400, "")
  end

  @doc """
  Handle incoming messages from telegram webhook
  """
  def dump(conn, params) do
    case VimwikiDeliver.Queue.dump(VimwikiDeliver.Queue) do
      dump = [_ | _] -> conn |> assign(:dump, dump) |> render("dump.txt")
      _ -> conn |> send_resp(204, "nothing to dump")
    end
  end
end
