defmodule VimwikiDeliverWeb.AdminView do
  use VimwikiDeliverWeb, :view
  use Timex

  def date do
    case DateTime.now("Europe/Berlin") do
      {:ok, dt} -> pretty_print_datetime(dt)
      _ -> {:error, "problem getting time"}
    end
  end

  def format_enteries(enteries), do: Enum.map(enteries, &(%{ txt_str: &1.entry_text, timestamp_str: pretty_print_datetime(&1.timestamp)}))

  defp pretty_print_datetime(dt) do
    {:ok, str} = Timex.format(dt, "%d-%m-%Y %H:%M", :strftime)
    str
  end
end
