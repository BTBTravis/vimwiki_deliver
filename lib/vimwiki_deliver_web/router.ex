defmodule VimwikiDeliverWeb.Router do
  use VimwikiDeliverWeb, :router

  def put_cors_header(conn, _) do
    Plug.Conn.put_resp_header(conn, "Access-Control-Allow-Origin", "*")
  end

  def has_correct_api_key(conn, _) do
    api_key = Application.get_env(:vimwiki_deliver, VimwikiDeliverWeb.Endpoint)[:api_key]
    case Plug.Conn.get_req_header(conn, "x-api-key") do
      [key] when key == api_key -> conn
      _ -> conn |> Plug.Conn.resp(401, "Unauthorized") |> Plug.Conn.send_resp() |> halt()
    end
  end

  pipeline :api do
    plug :accepts, ["json"]
    plug :put_cors_header
  end

  pipeline :admin do
    plug :has_correct_api_key
  end

  scope "/api", VimwikiDeliverWeb do
    pipe_through :api
    post "/message", TelegramController, :index
  end

  scope "/api/admin", VimwikiDeliverWeb do
    pipe_through :api
    pipe_through :admin
    get "/dump", AdminController, :dump
  end
end
