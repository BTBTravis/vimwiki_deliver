defmodule VimwikiDeliver.Sessions do
  @moduledoc """
  The Session context.
  """

  alias VimwikiDeliver.Sessions.Session

  @doc """
  create a session from telegram json
  """
  def create(raw_data) do
    with {:ok, user_id} <- get_user_id(raw_data),
	 {:ok, message} <- get_message(raw_data),
	 {:ok, chat_id} <- get_chat_id(raw_data)
    do
      {:ok, %Session{
        user_id: user_id,
        chat_id: chat_id,
        message: message,
        is_admin: is_admin(user_id),
        responces: [],
      }}
    end
  end

  @doc """
  add message to responces
  """
  def add_message(session, msg) do
    %Session{session | :responces => session.responces ++ [msg]}
  end

  # determin if admin from user_id
  defp is_admin(user_id) do
    admin_user_id = Application.get_env(:vimwiki_deliver, VimwikiDeliver.Sessions)[:admin_id]
    user_id == admin_user_id
  end

  # Gets message text from telegram json
  defp get_message(raw_data) do
    case get_in(raw_data, ["message", "text"]) do
      nil -> {:error, "can't find message in raw_data"}
      x -> {:ok, x}
    end
  end

  # Gets user id from telegram json
  defp get_user_id(raw_data) do
    case get_in(raw_data, ["message", "from", "id"]) do
      nil -> {:error, "can't find user_id in raw_data"}
      x -> {:ok, x}
    end
  end

  # Gets chat id from telegram json
  defp get_chat_id(raw_data) do
    case get_in(raw_data, ["message", "chat", "id"]) do
      nil -> {:error, "can't find user_id in raw_data"}
      x -> {:ok, x}
    end
  end
end
