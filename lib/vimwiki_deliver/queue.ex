defmodule VimwikiDeliver.Queue do
  use Agent

  @doc """
  Starts a new queue.
  """
  def start_link(ops) do
    Agent.start_link(fn -> [] end, ops)
  end

  @doc """
  Gets entire queue and empties it
  """
  def dump(queue) do
    data = Agent.get(queue, fn (x) -> x end)
    :ok = Agent.update(queue, fn (_x) -> [] end)
    data
  end

  @doc """
  Gets the number of queued enteries
  """
  def count(queue) do
    list = Agent.get(queue, fn (x) -> x end)
    length(list)
  end

  @doc """
  Puts the `entry` into the `queue`.
  """
  def put(queue, %VimwikiDeliver.Entries.Entry{} = entry) do
    Agent.update(queue, fn (list) -> list ++ [entry] end)
  end

  def put(queue, _) do
    {:error, "only entries can be added to a queue"}
  end
end
