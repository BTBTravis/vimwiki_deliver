defmodule VimwikiDeliver.Entries.Entry do
  @enforce_keys [:entry_text, :timestamp]
  defstruct [:entry_text, :timestamp]
end

