defmodule VimwikiDeliver.Sessions.Session do
  @enforce_keys [:user_id, :chat_id, :message, :is_admin]
  defstruct [:user_id, :chat_id, :message, :is_admin, :responces]
end
