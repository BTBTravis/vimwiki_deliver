defmodule VimwikiDeliver.Bot do
  alias VimwikiDeliver.{Sessions, Telegram, Queue, Entries}

  @doc """
  Runs telegram message through the app
  """
  def handle_incoming_telegram_msg(raw_data) do
    with {:ok, session} <- Sessions.create(raw_data),
         handle_fn <- (if session.is_admin, do: &handle_admin/1, else: &handle_non_admin/1)
    do
        handle_fn.(session)
    else
      _ -> {:error, "problem creating session"}
    end
  end

  # handle flow of non admin messages
  defp handle_non_admin(session) do
    Sessions.add_message(session, """
      Oops you are not an admin
      this bot only saves processes messages and saves them to vimwiki for admins
      """) |> Telegram.process_session
  end

  # handle flow of admin messages
  defp handle_admin(session) do
    discern_intent(session) |> Telegram.process_session
  end

  # Starts a new queue.
  defp discern_intent(session) do
    cond do
      Regex.match?(~r/^\/(start|help)/, session.message) -> send_help_msg(session)
      Regex.match?(~r/^\/count/, session.message) -> count_enteries(session)
      true -> save_entry(session)
    end
  end


  # Prints a help message explaining how to use the bot
  defp send_help_msg(session) do
    Sessions.add_message(session, """
      Hello I'm vimwiki delivery bot!
      Simply send me things and I'll hold on to them until you run the dump
      script on your laptop to retrive them

      Commands:
      /start -- prints this message
      /help  -- prints this message
      /count -- prints the amount of pending items waiting to be dumped to laptop
      ** -- all other messages will be concidered things to save to vimwiki
      """)
  end

  # saves message text to queue
  defp save_entry(session) do
    with {:ok, entry} <- Entries.create(session.message),
         :ok <- Queue.put(Queue, entry)
    do
      Sessions.add_message(session, "entry saved")
    else
      _ -> Sessions.add_message(session, "problem saving entry")
    end
  end

  defp count_enteries(session) do
    total = Queue.count(Queue)
    Sessions.add_message(session, "you have #{total} enteries waiting to be dumped to your laptop")
  end
end

