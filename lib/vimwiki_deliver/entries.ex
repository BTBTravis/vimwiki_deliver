defmodule VimwikiDeliver.Entries do
  alias VimwikiDeliver.Entries.Entry

  def create(txt) do
    case DateTime.now("Europe/Berlin") do
      {:ok, ts} -> {:ok, %Entry{
	  entry_text: txt,
	  timestamp: ts
	}}
      _ -> {:error, "problem getting time"}
    end
  end
end
