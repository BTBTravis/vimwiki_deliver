defmodule VimwikiDeliver.Telegram.Api do

  @http_client Application.get_env(:vimwiki_deliver, VimwikiDeliver.Telegram.Api)[:http_client]

  def send_msg(client, chat_id, msg) do
    path = "/sendMessage"
    @http_client.post(client, path, %{"text" => msg, "chat_id" => chat_id})
  end

  def client(token, url) do
    middleware = [
      {Tesla.Middleware.BaseUrl, url <> token},
      Tesla.Middleware.JSON,
      Tesla.Middleware.Logger
    ]

    Tesla.client(middleware)
  end
end
