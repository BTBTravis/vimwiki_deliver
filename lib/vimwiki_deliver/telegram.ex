defmodule VimwikiDeliver.Telegram do
  alias VimwikiDeliver.Telegram.Api

  defp get_token() do
    Application.get_env(:vimwiki_deliver, VimwikiDeliver.Telegram.Api)[:token]
  end

  defp get_base_url() do
    Application.get_env(:vimwiki_deliver, VimwikiDeliver.Telegram.Api)[:url]
  end

  defp send_message_to_chat(chat_id, msg) do
    Api.client(get_token(), get_base_url())
    |> Api.send_msg(chat_id, msg)
  end

  def process_session(session) do
    case session.responces do
      nil -> {:ok, "nothing to send"}
      responces -> Enum.map(responces, fn txt -> send_message_to_chat(session.chat_id, txt) end)
    end
  end
end

