#!/usr/bin/env perl
use strict;
use warnings;
use Getopt::Long qw(GetOptions);

# script arguments
my $path = '';
my $api_key = '';
my $host = '';
my $help = 0;

GetOptions(
    'path=s' => \$path,
    'apikey=s' => \$api_key,
    'host=s' => \$host,
    'help' => \$help,
) or die("Error in command line arguments\n");

die("missing --apikey") unless ($api_key ne '');
die("missing --path") unless ($path ne '');
die("missing --host") unless ($host ne '');

if ($help) {
    print "VimWiki Delivery Dumper
Queries server to get entries saved from Vimwiki Delivery Bot and appends them to Incoming.wiki
usage:
no arguments: runs all mobile uats test specs against localhost:3000
--path /../.vimwiki/Incoming.wiki   <> full path of file to append dump to
--apikey                            <> server api key xxxxxxxxx
--host                              <> server url
--help                              <> displays this text
\n";
    exit 0;
}

sub error_string {
    my $msg = shift;
    return "=== Error: $msg ===\n";
}

print "=== Curling Vimwiki Delivery Backend ===\n";
my $output = qx(curl --request GET \\
  --url ${host}/api/admin/dump \\
  --header 'x-api-key: ${api_key}' -s
);
die error_string('Problem getting dump from backend') unless ($? == 0);
my $l = length($output);

if ($l > 0) {
    print "=== Appending result to Incoming.wiki ===\n";
    open(my $fh, '>>', $path) or die "Could not open file '$path' $!";
    say $fh $output;
    close $fh;
}

print "=== DONE ===\n";
